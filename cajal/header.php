<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<!--Let browser know website is optimized for mobile-->
      	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<!-- Compiled and minified CSS -->
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    	<!--Import Google Icon Font-->
      	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      	<!--Extra css-->
      	<link type="text/css" rel="stylesheet" href="<?= get_stylesheet_directory_uri(); ?>/style.css"/>
      	<!--Import fonts-->
      	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap" rel="stylesheet">

		<?php wp_head(); ?>
	</head>
	<body>
		<div class="headerCajal <?= CFS()->get( 'background_color' );?>">
			
	
	

