$(document).ready(function(){

	$('.slider').slider({
		indicators: false,
		height: 500,
	});

	$( "#arrowRight" ).click(function() {
		$('.slider').slider('next');
	});
	$( "#arrowLeft" ).click(function() {
		$('.slider').slider('prev');
	});
	
	$('.sidenav').sidenav();

	$('.collapsible').collapsible({
		accordion: false,
		onOpenStart: function(evt){$(evt).find('img').addClass('openArrow')},
		onCloseStart:  function(evt){$(evt).find('img').removeClass('openArrow')},
	});

	$('.close-img').click(function() {
		$('.sidenav').sidenav('close');
	});
});