<?php 
	/*
  	Template Name: Home Cajal Tradicional
  	*/
  	get_header(); ?>
  	<?php include('menu.php'); ?>
	<div class="slider fullscreen">
	    	<div class="arrows">
	    		<img id="arrowLeft"  src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/back.svg">
				<img id="arrowRight" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/next.svg">
	    	</div>
	    <ul class="slides">
	      	<li>
	        	<img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/sliders/slider-01@2x.png">
	        	<div class="center button">
	        		<a class="btn-large waves-effect cta-slider btnColorSlider1"><span>QUIERO SABER</span></a>
	        	</div>
	      	</li>
	      	<li>
	        	<img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/sliders/slider-02@2x.png">
	        	<div class="center button">
	        		<a class="btn-large waves-effect cta-slider btnColorSlider2"><span>QUIERO SABER</span></a>
	        	</div>
	      	</li>
	    </ul>
	</div>
</div>
<div>
	<hr>
	<div class="row">
		<div class="col s6">
			<div class="botonera red">
				<h2>MEDICINA</h2>	
			</div>
		</div>
		<div class="col s6">
			<div class="botonera violet">
				<h2>FISIOTERAPIA</h2>
			</div>
		</div>
	</div>
	<hr>
</div>
<div class="medicina-img">
	<img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/8751.png">
	<h2 class="titleIntroMed">MEDICINA</h2>
	<p class="textIntroMed">Para comenzar esta etapa fundamental en tu vida, asegurá el éxito desde el comienzo, capacitándote con los mejores profesionales, que ponen a tu disposición su amplia experiencia docente, garantizando el mejor nivel académico y efectividad en tus exámenes.</p>
	<a class="waves-effect waves-light btn-large cta-slider btnColorSlider1">QUIERO SABER</a>
</div>
<div class="aulaVirtual">
	<img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/aula@2x.png">
</div>

<footer class="page-footer">
    <div class="row footerHome">
        <div class="col s4">
        	<img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/logoTop@2x.png">
        </div>
        <div class="col s6">
        	<p class="copyright-cajal">Ingreso y apoyo Universitario en Córdoba<br> Cajal Tradicional 2019</p>
        </div>
    </div>
</footer>


<?php get_footer(); ?>