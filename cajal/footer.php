    	<div class="fixed-action-btn">
		  	<a class="btn-floating btn-large">
		   			<img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/whatsapp.png">
		  	</a>
		</div>

		<footer class="page-footer grey darken-3">
          <div class="contentFooter">
			<a href="#!" class="brand-logo"><img class="logo-nav" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/logo-blanco.svg"></a>
            <div class="row">
              	<div class="col l3 s12">
					<ul>
						<li><a class="" href="#!">Inicio</a></li>
						<li><a class="" href="#!">Quienes Somos</a></li>
						<li><a class="" href="#!">Galeria</a></li>
						<li><a class="" href="#!">Aula Virtual</a></li>
						<li><a class="" href="#!">Blog</a></li>
						<li><a class="" href="#!">Preguntas Frecuentes</a></li>
						<li><a class="" href="#!">Contacto</a></li>
					</ul>
              	</div>
				<div class="col l3 s12">
					<ul>
						<li><a class="grey-text text-lighten-3" href="#!">CURSOS DE INGRESO</a></li>
						<li><a class="" href="#!">INGRESO MEDICINA UNC</a></li>
						<li><a class="" href="#!">INGRESO MEDICINA UCC</a></li>
						<li><a class="" href="#!">ANATOMÍA NORMAL</a></li>
						<li><a class="" href="#!">BIOQUÍMICA</a></li>
						<li><a class="" href="#!">NUTRICIÓN</a></li>
						<li><a class="" href="#!">FISIOTERAPIA Y KINESIOLOGÍA</a></li>
						<li><a class="" href="#!">ODONTOLOGÍA</a></li>
						<li><a class="" href="#!">CIENCIAS QUÍMICAS</a></li>
					</ul>
				</div>
				<div class="col l3 s12">
					<ul>
						<li><a class="grey-text text-lighten-3" href="#!">CURSOS CAPACITACIÓN LABORAL</a></li>
						<li><a class="" href="#!">Asistente en ÓPTICA</a></li>
						<li><a class="" href="#!">Asistente en FARMACIA</a></li>
						<li><a class="" href="#!">Asistente en ENFERMERÍA</a></li>
					</ul>
				</div>
				<div class="col l3 s12">
					<ul>
						<li><a class="" href="#!">Mapa del sitio</a></li>
						<li><a class="" href="#!">Política de privacidad y datos personales</a></li>
						<li><a class="" href="#!">Política de seguridad</a></li>
						<li><a class="" href="#!">Política de divulgación responsable</a></li>
						<li><a class="" href="#!">Términos y condiciones</a></li>
					</ul>
              	</div>       
			</div>
			<div class="footer-copyright">
				<div class="copy">
				Copyright Cajal Tradicional - Todos los derechos reservados
				</div>
			</div>
          </div>
        </footer>

    	<!-- Compiled and minified JavaScript -->
    	<script type="text/javascript" src="<?= get_stylesheet_directory_uri(); ?>/js/jquery-3.4.1.min.js"></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    	<script type="text/javascript" src="<?= get_stylesheet_directory_uri(); ?>/js/instance.js"></script>
    	<?php wp_footer(); ?>
    </body>
</html>	