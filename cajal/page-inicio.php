<?php 
	/*
  	Template Name: Inicio Cajal Tradicional
  	*/
  	get_header(); ?>
        <?php include('menu2.php'); ?>
        <div class="slider">
            <div class="arrows">
	    		<img id="arrowLeft"  src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/back.svg">
				<img id="arrowRight" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/next.svg">
	    	</div>
            <ul class="slides">
                <li>
                    <img src="<?= get_stylesheet_directory_uri(); ?>/img/sliders/slide-cajal-01.png"> <!-- random image -->
                </li>
                <li>
                    <img src="<?= get_stylesheet_directory_uri(); ?>/img/sliders/slide-cajal-01.png"> <!-- random image -->
                </li>
                <li>
                    <img src="<?= get_stylesheet_directory_uri(); ?>/img/sliders/slide-cajal-01.png"> <!-- random image -->
                </li>
                <li>
                    <img src="<?= get_stylesheet_directory_uri(); ?>/img/sliders/slide-cajal-01.png"> <!-- random image -->
                </li>
            </ul>
        </div>
        <div class="carreras">
            <div class="row">
                <div class="col s2 medicinaUNCBackground cuadroCurso">
                    <span class="fondoTextoCurso rojo"><p class="titleCurso">MEDICINA UNC</p></span>
                    <img class="iconosCarreras" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/ico-medicina-ucc.svg"/>
                </div>
                <div class="col s2 fisioterapiaBackground cuadroCurso">
                    <span class="fondoTextoCurso azul"><p class="titleCurso">FISIOTERAPIA Y KINESIOLOGÍA</p></span>
                    <img class="iconosCarreras" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/ico-fisioterapia.svg">
                </div>
                <div class="col s2 quimicaBackground cuadroCurso">
                    <span class="fondoTextoCurso verde"><p class="titleCurso">BIOQUÍMICA</p></span>
                    <img class="iconosCarreras" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/ico-quimica.svg">
                </div>
                <div class="col s2 nutricionBackground cuadroCurso">
                    <span class="fondoTextoCurso amarillo"><p class="titleCurso">NUTRICIÓN</p></span>
                    <img class="iconosCarreras iconNutricion" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/ico-nutricion.svg">
                </div>
                <div class="col s2 odontologiaBackground cuadroCurso">
                    <span class="fondoTextoCurso celeste"><p class="titleCurso">ODONTOLOGÍA</p></span>
                    <img class="iconosCarreras" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/ico-odontologia.svg">
                </div>
                <div class="col s2 medicinaUCCBackground cuadroCurso">
                    <span class="fondoTextoCurso rojoucc"><p class="titleCurso">MEDICINA UCC</p></span>
                    <img class="iconosCarreras" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/ico-medicina-ucc.svg">
                </div>
            </div>
        </div> 
    </div>

    <div class="containerSeccion">
        <div class="fortalezas">
            <h3 class="textFortalezas rojo">Razones para elegirnos:</h3>
            <hr class="lineCTA rojo">
            <div class="row">
                <div class="col s1">
                </div>
                <div class="col s2">
                    <img class="iconosRazones" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/ico-razones-01.svg">   
                    <p class="rojo">Más de 50 años de experiencia en ingresos universitarios avalan nuestros resultados</p>
                </div>
                <div class="col s2">
                    <img class="iconosRazones" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/ico-razones-02.svg">
                    <p class="rojo">Somos la unica institución de Córdoba especializada en ingresos universitarios al área de la salud</p>
                </div>
                <div class="col s2">
                    <img class="iconosRazones" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/ico-razones-03.svg">
                    <p class="rojo">Gran plantilla de docentes de primer nivel con basta experiencia en docencia universitaria</p>
                </div>
                <div class="col s2">
                    <img class="iconosRazones" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/ico-razones-04.svg">
                    <p class="rojo">Miles de nuestros alumnnos alcanzaron su objetivo de ingresar a la facultad y culminar sus estudios</p>
                </div>
                <div class="col s2">
                    <img class="iconosRazones" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/ico-razones-05.svg">
                    <p class="rojo">Nuestros alumnos hoy ejercen en reconocidas instituciones del pais y el mundo</p>
                </div>
            </div>
        </div>
    </div>
    <div class="medicinaSeccion">
                <div class="row">
                    <div class="col s6">
                        <div class="containerTextoIngreso">
                            <h1 class="tituloSeccion rojo">MEDICINA</h1>
                            <hr class="lineSeccion rojo">
                            <p class="titleCTA">¿Queres ingresar a la carrera de<br> medicina en la UNC o en la UCC?</p>
                            <p class="response">Esta es la info que necesitas saber para<br> programar tu ingreso de la mejor forma:</p>
                            <a class="waves-effect waves-light btn carrerasCTA rojoCTA">Quiero saber más sobre<br> los cursos de ingreso a medicina</a>
                        </div>
                    </div>
                    <div class="col s6">
                        <img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/1113.png"> 
                    </div>
                </div>
                <hr class="lineCTA rojo">
    </div>

    <div class="containerSeccion">
        <div class="row">
            <div class="col s6">
                <img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/252.png">
                <h2 class="tituloSeccion violeta fonth2">KINESIOLOGÍA<br> Y FISIOTERAPIA</h2>
                <hr class="lineSeccion violeta">
                <p class="response">¿Buscas información sobre cómo prepararte para<br>
                 ingresar a la Escuela de Kinesiología y Fisioterapia de<br> la UNC? Haz clic aquí para saber más:</p>
                <a class="waves-effect waves-light btn carrerasCTA violetaCTA">Quiero saber más sobre los cursos de<br> ingreso a Kinesiología y Fisioterapia</a>
            </div>
            <div class="col s6">
                <img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/3998.png">
                <h2 class="tituloSeccion amarillo fonth2">NUTRICIÓN</h2>
                <hr class="lineSeccion amarillo">
                <p class="response"><br><br>¿Necesitas preparar tu ingreso a Nutrición?<br> Clic aquí para conocer toda la información sobre los cursos y<br> modalidades disponibles:</p>
                <a class="waves-effect waves-light btn carrerasCTA amarilloCTA">Quiero saber más sobre los<br> cursos de ingreso a Nutrición</a>
            </div>
        </div>
    </div>

    <div class="cursosLaboral">
        <hr class="lineCTA rojo">
        <h2 class="tituloSeccionCursos gris">¿ESTÁS BUSCANDO CURSOS <br> DE CAPACITACIÓN LABORAL?</h2>
        <p class="response">Estos tres cursos te van a brindar la capacitación que necesitas<br> para insertarte rápidamente en el mercado laboral.</p>
        <div class="row">
            <div class="col s3">
            </div>
            <div class="col s2">
                <img class="iconosCursoLaboral" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/ico-optica.svg">
                <h3 class="titleCursosHome mostaza">Asistente en<br> <b>ÓPTICA</b></h3>
            </div>
            <div class="col s2">
                <img class="iconosCursoLaboral" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/ico-farmacia.svg">
                <h3 class="titleCursosHome magenta">Asistente en<br> <b>FARMACIA</b></h3>
            </div>
            <div class="col s2">
                <img class="iconosCursoLaboral" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/ico-enfermeria.svg">
                <h3 class="titleCursosHome verdeAgua">Asistente en<br> <b>ENFERMERÍA</b></h3>
            </div>
            <div class="col s3">
            </div>
        </div>			
	</div>


<?php get_footer(); ?>