<?php 
	/*
  	Template Name: Contacto Cajal Tradicional
  	*/
  	get_header(); ?>
  			<?php include('menu-black.php'); ?>
  			<div class="volver">
		  		<img class="arrowBack" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/left-arrow.svg">
		  	</div>

		  	<div class="container">
		  		<h1 class="titleContacto">CONTACTO</h1>

		  		<div class="formContacto">
		  			<form id="formConsulta" class="col s12">
	                  	<div class="row">
		                    <div class="input-field col s12">
		                      	<input id="icon_prefix" name="nombreInput" type="text" class="validate">
		                      	<label for="icon_prefix">Nombre</label>
		                      	<span class="helper-text" data-error="Por favor, ingrese un nombre." data-success="" />
		                    </div>
		                    <div class="input-field col s12">
		                      	<input id="icon_telephone" name="telefonoInput" type="tel" class="validate">
		                      	<label for="icon_telephone">Telefono</label>
		                      	<span class="helper-text" data-error="Por favor, ingrese un telefono." data-success="" />
		                    </div>
		                    <div class="input-field col s12">
		                      	<input id="email" name="emailInput" type="email" class="validate">
		                      	<label for="email">Email</label>
		                      	<span class="helper-text" data-error="Por favor, ingrese un email válido." data-success="">Ej: ejemplo@ejemplo.com</span>
		                    </div>
		                    <div class="input-field col s12">
		                      	<textarea id="icon_prefix2" class="materialize-textarea"></textarea>
		                      	<label for="icon_prefix2">Deje su consulta</label>
		                    </div>
		                    <button class="btn-large waves-effect cta-form btnColorSlider1" type="submit" name="action">
		                    	ENVIAR MENSAJE
		                    </button>
	                  	</div>
	                </form>
		  		</div>

		  		<div class="ubicacion">
		  			<h2 class="titleUbicacion">UBICACIÓN</h2>
		  			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13617.901994992684!2d-64.1876719!3d-31.428574!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbe0f0275a7280b0e!2sCajal%20Tradicional%20-%20Ingreso%20a%20Medicina%20y%20apoyo%20Universitario%20en%20C%C3%B3rdoba!5e0!3m2!1ses!2sar!4v1573532459266!5m2!1ses!2sar" width="327" height="327" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
		  		</div>

		  	</div>

			<footer class="page-footer">
			    <div class="row footerHome">
			        <div class="col s4">
			        	<img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/logoTop@2x.png">
			        </div>
			        <div class="col s6">
			        	<p class="copyright-cajal">Ingreso y apoyo Universitario en Córdoba<br> Cajal Tradicional 2019</p>
			        </div>
			    </div>
			</footer>

		</div>

<?php get_footer(); ?>