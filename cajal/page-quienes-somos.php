<?php 
	/*
  	Template Name: Quienes somos Cajal Tradicional
  	*/
  	get_header(); ?>
  			<?php include('menu2.php'); ?>
                <div class="container containerSeccion">
					<div class="row">
						<div class="col l6 s12"> 
                            <img class="imageSeccion" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/ico-somos-seccion.svg">
						</div>
						<div class="col l6 s12"> 
							<h1 class="tituloSeccion blanco">QUIENES SOMOS</h1>
							<hr class="lineSeccion blanco">
							
							<p class="reseñaSeccion">Nuestra historia comienza hace más de 50 años cuando entendimos que, en la carrera de Medicina, 
                                los estudiantes necesitaban apoyo en dos de los pilares fundamentales de la carrera como lo son Anatomía e Histología. 
                                Con una gran aceptación y con el paso de los años comenzamos a incorporar nuevas materias como Fisiología, Química Biológica y 
                                Anatomía Patológica, entre otras no menos importantes, que nos permitieron estar siempre preparados acorde a lo que nuestros alumnos necesitaban.<br><br> 
                                Fieles a nuestra historia, hoy en día nuestra actividad está principalmente ligada al ingreso universitario en carreras del área de la salud, 
                                apoyando a cientos de jóvenes estudiantes año tras año.
							</p>

						</div>
					</div>
				</div>
        </div>

        <div class="medicinaSeccion">
                <div class="row">
                    <div class="col s6">
                        <div class="containerTextoIngreso">
                            <h1 class="tituloSeccion amarillo2">TRAYECTORIA</h1>
                            <hr class="lineSeccion amarillo2">
                            <p class="response">Con el objetivo y el deseo de formar futuros profesionales que lleven lejos su vocación, 
                            continuamos ampliando nuestra oferta de servicios en las diferentes etapas del proceso, que hacen al crecimiento personal, profesional y humano.<br><br>
                            Nuestra trayectoria está respaldada por la gran cantidad de alumnos de todo el país, incluso del exterior, que año tras año deciden prepararse en 
                            nuestra institución. Gracias a esto y a nuestros más de 50 años en el sector, muchos de nuestros alumnos 
                            (hoy eximios profesionales del Área de la Salud) nos recomiendan día a día.<br><br>
                            Así, podemos afirmar con orgullo, que hoy y desde hace mucho tiempo Cajal Tradicional es el líder absoluto en el ingreso a carreras del 
                            Área de la Salud.
                            </p>
                        </div>
                    </div>
                    <div class="col s6">
                        <img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/2416.png"> 
                    </div>
                </div>
                <hr class="lineCTA amarillo2">
        </div>
        
        <div class="contenidoSeccion">
            <div class="row">
                <h3 class="titleOptica amarillo2">NUESTROS DOCENTES</h3>
                <hr class="lineSeccion amarillo2">
                <div class="col s6">
                    <p class="response">Creemos en una relación entre docente-alumno, basada en la actitud positiva y el respeto mutuo.<br><br>
                        Flexibilidad, autocrítica y espíritu de superación permanente, son las características de todos nuestros profesores, 
                        que conforman un equipo de profesionales con un excelente nivel pedagógico, que hacen de la clase, la principal instancia de aprendizaje.</p>

                </div>
                <div class="col s6">
                    <p class="response">Formamos alumnos entusiastas y optimistas, que sueñan con llegar lejos en su profesión y ocupar lugares de privilegio 
                        en el Área de la Salud.<br><br>
                        Enseñamos a conocer, a detectar y a corregir sus límites, como así también a potenciar sus virtudes.<br><br>
                        Porque en Cajal Tradicional, creemos que llegar más lejos, también es un aprendizaje.</p>
                </div>
            </div>
		</div>

<?php get_footer(); ?>