<div class="menu">
	<ul id="slide-out" class="sidenav">
		<div class="close-menu"><img class="close-img" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/wrong.png"></div>
	    <ul class="collapsible">
            <li><a class="collapsible-header" href=""><i class="small material-icons colorGrey">home</i> INICIO</a></li>
            <li><a class="collapsible-header" href=""><i class="small material-icons colorGrey">people_outline</i> SOMOS</a></li>
            <li>
              <div class="collapsible-header colorGrey fontWeight500"><i class="small material-icons cursosIcon colorGrey">edit</i> CURSOS</div>
              <div class="collapsible-body">
                <ul>
                	<li><a href="">Publicidad Online</a></li>
          			<li><a href="">Social Media</a></li>
          			<li><a href="">Sitios web</a></li>
          			<li><a href="">Email Marketing</a></li>
          			<li><a href="">Posicionamiento en buscadores</a></li>
          			<li><a href="">E-commerce</a></li>
                </ul>
              </div>
            </li>
            <li><a class="collapsible-header" href=""><i class="small material-icons colorGrey">camera_alt</i> GALERIA</a></li>
            <li><a class="collapsible-header" href=""><i class="small material-icons colorGrey">mail_outline</i> CONTACTO</a></li>
            <li><a class="collapsible-header" href=""><i class="small material-icons colorGrey">laptop</i> AULA VIRTUAL</a></li>
        </ul>
	</ul>
	<a href="#" data-target="slide-out" class="sidenav-trigger menuMobile whiteFont"><img src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/menu.svg" width="35"></a>
	<img class="logo" src="<?= get_stylesheet_directory_uri(); ?>/img/logoTop@2x.png">
	<a href="#" class="contactoMobile whiteFont"><i class="small material-icons">email</i></a>

</div>