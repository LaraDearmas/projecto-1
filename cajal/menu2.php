<div class="menu">
  <nav>
    <div class="nav-wrapper">
      <a href="#!" class="brand-logo"><img class="logo-nav" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/logo-blanco.svg"></a>
      <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="sass.html">INICIO</a></li>
        <li><a href="badges.html">CURSOS DE INGRESO</a></li>
        <li><a href="collapsible.html">GALERIA</a></li>
        <li><a href="mobile.html">SOMOS</a></li>
        <li><a href="badges.html">FAQ'S</a></li>
        <li><a href="collapsible.html">AULA VIRTUAL</a></li>
        <li><a href="mobile.html">CONTACTO</a></li>
      </ul>
    </div>
  </nav>

  <ul class="sidenav" id="mobile-demo">
    <li><a href="sass.html">INICIO</a></li>
    <li><a href="badges.html">CURSOS DE INGRESO</a></li>
    <li><a href="collapsible.html">GALERIA</a></li>
    <li><a href="mobile.html">SOMOS</a></li>
    <li><a href="badges.html">FAQ'S</a></li>
    <li><a href="collapsible.html">AULA VIRTUAL</a></li>
    <li><a href="mobile.html">CONTACTO</a></li>
  </ul>
</div>