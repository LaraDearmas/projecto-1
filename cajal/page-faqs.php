<?php 
	/*
  	Template Name: Faqs Cajal Tradicional
  	*/
  	get_header(); ?>
  			<?php include('menu2.php'); ?>
                <div class="container containerSeccion">
					<div class="row">
						<div class="col l6 s12"> 
							<img class="imageSeccion" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/ico-preguntas-seccion.svg">
						</div>
						<div class="col l6 s12"> 
							<h1 class="tituloSeccion blanco">PREGUNTAS <br> FRECUENTES</h1>
							<hr class="lineSeccion blanco">
							
							<p class="reseñaSeccion"><span class="tituloReseña">¿Tenes alguna duda sobre nuestros cursos?</span><br><br>
								Antes de invertir en cualquier formación tradicional u online siempre es 
								importante responder a esas preguntas que casi por defecto salen de nuestra mente. Sabemos que obteniendo respuestas 
								a estas preguntas vamos a estar tranquilos de que estamos por formarnos en el lugar correcto y con el equipo correcto 4
								de profesionales.<br><br>
								Dicho esto, aquí es el lugar donde encontrarás respuesta a algunas de las preguntas que de manera más 
								frecuente nos han hecho sobre nuestros cursos, y probablemente sean las que vos ahora mismo estás planteandote:
							</p>

						</div>
					</div>
				</div>
		</div>

		<div class="contenidoSeccion">
					<div>
						<ul class="collapsible expandable preguntasFrecuentes">
							<li>
								<div class="collapsible-header question"><div>¿Tengo que contar con una base de conocimientos previos para realizar el curso?</div><img class="imgArrow" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/down-arrow.svg"></div>
								<div class="collapsible-body"><span class="response">En nuestros cursos, partimos de cero en todas las materias, teniendo en cuenta el programa de la Universidad. Es cierto que 
									si ya contás con una base de conocimientos, el proceso de aprendizaje será más simple y rápido, pero no es un requisito.</span></div>
							</li>
							<li>
								<div class="collapsible-header question"><div>¿Si hago el curso, tengo el ingreso asegurado?</div><img class="imgArrow" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/down-arrow.svg"></div>
								<div class="collapsible-body"><span class="response"> En Cajal te brindamos todas las herramientas necesarias para cumplir con el objetivo de ingresar a la 
									Facultad, pero como bien se sabe, los resultados dependen en un 50% de nosotros y el otro 50% depende de vos.</span></div>
							</li>
							<li>
								<div class="collapsible-header question"><div>¿Qué beneficios tengo por ser o haber sido alumno?</div><img class="imgArrow" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/down-arrow.svg"></div>
								<div class="collapsible-body"><span class="response"> Por ser o haber sido alumno, tenés descuentos en otros cursos de apoyo a lo largo de tu carrera.</span></div>
							</li>
							<li>
								<div class="collapsible-header question"><div>¿Tienen las preguntas de examen de la Universidad Nacional y de la Universidad<br> Católica de Córdoba?</div><img class="imgArrow" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/down-arrow.svg"></div>
								<div class="collapsible-body"><span class="response">La información de los exámenes de las Universidades es totalmente confidencial, y el acceso a los mismos está 
									fuera de nuestro alcance, es por ello que preparamos y exigimos tanto en el rendimiento y carga horaria a nuestros alumnos.
									El sistema de evaluación es de similar complejidad al de la Universidad y te preparamos para afrontarlo con éxito.</span></div>
							</li>
							<li>
								<div class="collapsible-header question"><div>¿Hay otras sucursales o franquicias en el país?</div><img class="imgArrow" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/down-arrow.svg"></div>
								<div class="collapsible-body"><span class="response"> Por el momento contamos con una única sede situada en la Ciudad de Córdoba, Argentina.</span></div>
							</li>
							<li>
								<div class="collapsible-header question"><div>¿Qué modalidad de curso me conviene hacer?</div><img class="imgArrow" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/down-arrow.svg"></div>
								<div class="collapsible-body"><span class="response"> Todos nuestros cursos son de excelente calidad. La diferencia es la duración del mismo y la rapidez en 
									la que se ven los contenidos. Cada año nuestros alumnos de diferentes modalidades cumplen con su objetivo de ingresar a la Facultad.</span></div>
							</li>
							<li>
								<div class="collapsible-header question"><div>¿Si hago un curso para la Universidad Nacional, me sirve para la Universidad Católica<br> de Córdoba y viceversa?</div><img class="imgArrow" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/down-arrow.svg"></div>
								<div class="collapsible-body"><span class="response"> Las Universidades tienen materias en común, pero los contenidos, los autores y fechas de examen son diferentes. 
									Si bien hay alumnos que se han preparado para la Universidad Nacional y han rendido bien en la Universidad Católica, son dos cursos diferentes, 
									es por ello que contamos con cursos específicos para cada Universidad.</span></div>
							</li>
							<li>
								<div class="collapsible-header question"><div>¿Son muy costosos los cursos de Cajal?</div><img class="imgArrow" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/down-arrow.svg"></div>
								<div class="collapsible-body"><span class="response">Teniendo en cuenta la carga horaria, que el material de estudio está incluido en el costo del curso, 
									los talleres de manejo de ansiedad, y que te acompañamos hasta que rindas el examen con las clases de consulta de refuerzo del verano y 
									la posibilidad de volver a realizar nuevos exámenes, el costo de la hora de estudio es menor al que podés conseguir por estos servicios en 
									cualquier otro lugar.</span></div>
							</li>
						</ul>
					</div>
					<div class="faqsCTA">
						<hr class="lineCTA verde">
						<p class="titleCTA">¿Tenés alguna consulta?<br>
						¿Querés recibir info completa sobre los cursos de ingreso a Medicina UCC?</p>
						<a class="waves-effect waves-light btn CTA verdeCTA">SI QUIERO ENVIAR UNA <br>CONSULTA ESPECIAL</a>
					</div>
				</div>

<?php get_footer(); ?>