<?php 
	/*
  	Template Name: Contacto2 Cajal Tradicional
  	*/
  	get_header(); ?>
  			<?php include('menu2.php'); ?>
                <div class="container containerSeccion">
					<div class="row">
						<div class="col l6 s12"> 
                            <img class="imageSeccion" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/ico-contacto-seccion.svg">
						</div>
						<div class="col l6 s12"> 
							<h1 class="tituloSeccion blanco">CONTACTO</h1>
							<hr class="lineSeccion blanco">
							
							<p class="reseñaSeccion">Nos darías una gran noticia si nos dijeras que, después de ver nuestra web, 
                                te diste cuenta de todo lo que podes aprender preparandote junto a nosotros.<br><br>
                                De ser así estaremos encantados de responder cualquiera de las preguntas que quieras hacernos. 
                                Solo tenés que completar el formulario con tus datos, enviarnos tu consulta, y rápidamente tendrás nuestra respuesta en tu bandeja de entrada.
							</p>

						</div>
					</div>
				</div>
        </div>

		<div class="contenidoSeccion">
            <div class="row">
                <div class="col l7">
                    <div class="formContacto">
                        <h3 class="titleOptica marron">¿EN QUÉ PODEMOS AYUDARTE?</h3>
                        <form id="formContacto" class="col s12">
                            <div class="row">
                                <div class="input-field col s12">
                                        <input id="icon_prefix" name="nombreInput" type="text" class="validate">
                                        <label for="icon_prefix">NOMBRE</label>
                                        <span class="helper-text" data-error="Por favor, ingrese un nombre." data-success="" />
                                </div>
                                <div class="input-field col s12">
                                        <input id="icon_telephone" name="telefonoInput" type="tel" class="validate">
                                        <label for="icon_telephone">TELÉFONO</label>
                                        <span class="helper-text" data-error="Por favor, ingrese un telefono." data-success="" />
                                </div>
                                <div class="input-field col s12">
                                        <input id="email" name="emailInput" type="email" class="validate">
                                        <label for="email">EMAIL</label>
                                        <span class="helper-text" data-error="Por favor, ingrese un email válido." data-success="">Ej: ejemplo@ejemplo.com</span>
                                </div>
                                <div class="input-field col s12">
                                        <textarea id="icon_prefix2" class="materialize-textarea"></textarea>
                                        <label for="icon_prefix2">MENSAJE</label>
                                </div>
                                <button class="btn-large waves-effect cta-form contactoBackground" type="submit" name="action">
                                    ENVIAR MI CONSULTA
                                </button>
                            </div>
                        </form> 
                    </div>
                </div>
                <div class="col l5">
                    <h3 class="titleOptica marron">¿DÓNDE ESTAMOS?</h3>
                    <p class="response"><b>ÚNICA DIRECCIÓN:</b><br>
                        Buenos Aires 1057 - Nueva Córdoba (Provincia de Córdoba)
                    </p>
                    <p class="response"><b>¿Te gustaría hablar con nosotros?</b><br>
                        Estos son nuestros números de contacto:<br>
                         Tel/Fax: 0351 – 4685956 | 0351 - 4600277 | 0351 - 4608419
                    </p>
                    <p class="response"><b>¿Preferís comunicarte por WhatsApp?</b></p>
                    <a class="waves-effect waves-light blanco CTAWhatsapp contactoBackground">
                        <img class="imgWhatsapp" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/whatsapp.png">Clic Aquí para enviarnos un WhatsApp.
                    </a>
                    <p class="response"><b>¿En qué horarios puedo encontrarlos para resolver mis consultas o asistir a las oficinas?</b><br><br>
                        Estos son nuestros HORARIOS DE ATENCIÓN:<br><br>
                        Informes: Lunes a Viernes de 9:00 a 19:30 hs. / Sábados de<br> 9:00 a 16:00 hs.<br>
                        Administración: Lunes a Viernes de 9:00 a 13:00 hs. y de 15:00 a 19:30 hs. / Sábados de 9:00 a 13:00 hs.
                    </p>

                </div>
            </div>
		</div>

		<div class="ubicacion">
            <div class="titleUbicacion">
                <h3 class="titleOptica marron">CÓMO LLEGAR</h3>
                <hr class="lineSeccion marron">
            </div>
		  	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13617.901994992684!2d-64.1876719!3d-31.428574!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbe0f0275a7280b0e!2sCajal%20Tradicional%20-%20Ingreso%20a%20Medicina%20y%20apoyo%20Universitario%20en%20C%C3%B3rdoba!5e0!3m2!1ses!2sar!4v1573532459266!5m2!1ses!2sar" frameborder="0" style="border:0;" class="mapa" allowfullscreen=""></iframe>
		</div>


<?php get_footer(); ?>