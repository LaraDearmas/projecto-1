<?php 
	/*
  	Template Name: Galeria Cajal Tradicional
  	*/
  	get_header(); ?>
  			<?php include('menu2.php'); ?>
                <div class="container containerSeccion">
					<div class="row">
						<div class="col l6 s12"> 
                            
						</div>
						<div class="col l6 s12"> 
							<h1 class="tituloSeccion blanco">GALERÍA</h1>
							<hr class="lineSeccion blanco">
							
							<p class="reseñaSeccion">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. 
                                Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor 
                                (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que
                                 logró hacer un libro de textos especimen.
							</p>

						</div>
					</div>
				</div>
        </div>

        <div class="medicinaSeccion">
                <div class="row">
                    <div class="col l3 columnaGaleria">
                        <a href="">
                            <div class="panelFoto">
                                <img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/galeria/20190919_103741.png"> 
                                <div class="seccionPanel">
                                    <img class="plus" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/mas.svg">
                                    <h2 class="tituloGaleria musgo">EXTERIOR</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col l3 columnaGaleria">
                        <a href="">
                            <div class="panelFoto">
                                <img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/galeria/20190919_102642.png"> 
                                <div class="seccionPanel">
                                    <img class="plus" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/mas.svg">
                                    <h2 class="tituloGaleria musgo">INTERIOR</h2>
                                </div>
                            </div>
                        </a>  
                    </div>
                    <div class="col l3 columnaGaleria">
                        <a href="">
                            <div class="panelFoto">
                                <img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/galeria/20190919_102111.png"> 
                                <div class="seccionPanel">
                                    <img class="plus" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/mas.svg">
                                    <h2 class="tituloGaleria musgo">CURSOS</h2>
                                </div>
                            </div>
                        </a>  
                    </div>
                    <div class="col l3 columnaGaleria">
                        <a href="">
                            <div class="panelFoto">
                                <img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/galeria/20190919_102310.png"> 
                                <div class="seccionPanel">
                                    <img class="plus" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/mas.svg">
                                    <h2 class="tituloGaleria musgo">AULAS</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                
                    <div class="col l3 columnaGaleria">
                        <a href="">
                            <div class="panelFoto">
                                <img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/galeria/DSC_0050-2.png"> 
                                <div class="seccionPanel">
                                    <img class="plus" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/mas.svg">
                                    <h2 class="tituloGaleria musgo">PATIO</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col l3 columnaGaleria">
                        <a href="">
                            <div class="panelFoto">
                                <img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/galeria/20190919_103741.png"> 
                                <div class="seccionPanel">
                                    <img class="plus" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/mas.svg">
                                    <h2 class="tituloGaleria musgo">DESAFÍO</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col l3 columnaGaleria">
                        <a href="">
                            <div class="panelFoto">
                                <img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/galeria/DSC_0050-2.png"> 
                                <div class="seccionPanel">
                                    <img class="plus" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/mas.svg">
                                    <h2 class="tituloGaleria musgo">JORNADA</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col l3">
                        <div class="panelFoto">
                            <!--<img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/galeria/DSC_0050-2.png"> 
                            <div class="seccionPanel">
                                <i class="large material-icons musgo">add_circle</i>
                                <h2 class="tituloGaleria musgo">EXTERIOR</h2>
                            </div>-->
                        </div>
                    </div>
                </div>
        </div>
        

<?php get_footer(); ?>