<?php 
	/*
  	Template Name: Cursos Cajal Tradicional
  	*/
  	get_header(); ?>
  			<?php include('menu2.php'); ?>
                <div class="containerSeccion">
					<div class="row">
						<div class="col l6 s12"> 
                            <h1 class="tituloSeccion blanco">ASISTENTE<br> EN ÓPTICA</h1>
                            <hr class="lineSeccion blanco">
                            
                            <p class="reseñaSeccion">Si lo que estás buscando es adquirir nuevas habilidades que te permitan insertarte 
                                rápidamente en el mercado laboral, nuestro Curso de Asistente en Óptica te permitirá tener las habilidades personales y profesionales 
                                suficientes para que rápidamente puedas acceder a un puesto de trabajo en establecimientos de óptica oftálmica,de instrumental óptico, 
                                laboratorios y talleres ópticos donde podrás acompañar y asistir a los profesionales universitarios en todas las tareas diarias que requiere 
                                la óptica.
                            </p>
						</div>
						<div class="col l6 s12"> 
							<div class="formCursos">
								<form id="formCursos" class="col s12">
									<h2 class="titleFormSeccion">¿NECESITAS MÁS INFORMACIÓN SOBRE COMO ACCEDER A NUESTRO CURSO DE ASISTENTE EN ÓPTICA?</h2>
									<div class="row">
										<div class="input-field col s12">
											<input id="icon_prefix" name="nombreInput" type="text" class="validate">
											<label for="icon_prefix">NOMBRE</label>
											<span class="helper-text" data-error="Por favor, ingrese un nombre." data-success="" />
										</div>
										<div class="input-field col s12">
											<input id="icon_telephone" name="telefonoInput" type="tel" class="validate">
											<label for="icon_telephone">TELÉFONO</label>
											<span class="helper-text" data-error="Por favor, ingrese un telefono." data-success="" />
										</div>
										<div class="input-field col s12">
											<input id="email" name="emailInput" type="email" class="validate">
											<label for="email">EMAIL</label>
										</div>
										<div class="input-field col s12">
											<textarea id="icon_prefix2" class="materialize-textarea"></textarea>
											<label for="icon_prefix2">MENSAJE</label>
										</div>
										<button class="btn-large waves-effect ctaFormCursos naranjaCTA" type="submit" name="action">
											ENVIAR CONSULTA
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
				<div>
                 <img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/cursos/2496228.png">
                </div>
				<div class="contenidoSeccion">
					<div>
                        <h3 class="titleOptica naranja">Entre las habilidades que desarrollarás<br> en esta formación se destacan:</h3>
                        <p class="response">Conocimientos básicos sobre el proceso de la visión, sus alteraciones y medios de protección y correctivos.<br><br>
                            Conocimientos básicos de Óptica, relacionados con la integridad de la visión de las personas, para poder colaborar en la confección 
                            y adaptación de las lentes necesarias a cada caso, sean de corrección y/o de protección.<br><br>
                            Colaborar en la elaboración de productos ópticos, según las normas de calidad, seguridad e higiene vigentes, y manejo adecuado de residuos y elementos contaminantes.</p>
					</div>
					<div>
                        <h3 class="titleOptica naranja">Además este curso incluye un módulo<br> exclusivo en donde aprenderás a:</h3>
                        <p class="response">Confeccionar tu CV para volverlo eficaz al momento de comenzar a realizar tu búsqueda laboral y reforzar tus habilidades personales y profesionales 
							de tal forma que estés preparado para asistir a una Entrevista en el Sector laboral relacionado a la Óptica.</p>
					</div>
					<div>
                        <h3 class="titleOptica naranja">¿A que voy a tener acceso si me inscribo <br>al Curso de Asistente en Óptica?</h3>
                        <p class="response"><i class="material-icons iconsCurso naranja">brightness_1</i> Clases teóricas, expositivas y multimediales, a cargo de nuestros profesionales.</p>
						<p class="response"><i class="material-icons iconsCurso naranja">brightness_1</i> Clases prácticas sobre los diferentes procedimientos que hacen al manejo de las destrezas y habilidades necesarias para abordar con eficiencia las diversas situaciones que se viven en el ámbito de la óptica.</p>
						<p class="response"><i class="material-icons iconsCurso naranja">brightness_1</i> Presentaciones de casos conflictivos en donde se debatirá sobre cuál es la manera correcta de dar solución ante un conflicto, de tal forma que estés preparado para resolverlo cuando estés desarrollando tu actividad profesional.</p>
						<p class="response"><i class="material-icons iconsCurso naranja">brightness_1</i> Evaluaciones mensuales online y una evaluación teórica y práctica final para certificar las competencias adquiridas.</p>
						<p class="response"><i class="material-icons iconsCurso naranja">brightness_1</i> Encuentro final de formación para perfeccionar tu búsqueda de trabajo y tus competencias para la inserción en el ámbito laboral.</p>
					</div>
					<div class="modalidadCurso">
						<h3 class="titleOptica naranja">MODALIDAD de cursado:</h3>
						<p class="response2"><i class="material-icons iconsCurso2">brightness_1</i> <b>Duración:</b> 4 meses</p>
						<p class="response2"><i class="material-icons iconsCurso2">brightness_1</i> <b>Inicio:</b> Agosto 2019.</p>
						<p class="response2"><i class="material-icons iconsCurso2">brightness_1</i> <b>Finalización:</b> Diciembre 2019.</p>
						<p class="response2"><i class="material-icons iconsCurso2">brightness_1</i> <b>Modalidad:</b> Clases presenciales de 3 horas <br>a dictarse 1 vez por semana.<br><br></p>
						<p class="response2">(consultar nuevas fechas de inicio)</p>
					</div>
					<div class="faqsCTA">
						<hr class="lineCTA naranja">
						<p class="titleCTA">¿Tenés alguna consulta?<br>
						¿Querés recibir info completa sobre los cursos de ingreso a Medicina UCC?</p>
						<a class="waves-effect waves-light btn CTA naranjaCTA">SI QUIERO RECIBIR MÁS INFORMACIÓN<br> SOBRE EL CURSO DE<br> ASISTENTE EN ÓPTICA</a>
					</div>
				</div>

<?php get_footer(); ?>