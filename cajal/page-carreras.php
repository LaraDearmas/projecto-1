<?php 
	/*
  	Template Name: Carreras Cajal Tradicional
  	*/
  	get_header(); ?>
  			<?php include('menu2.php'); ?>
                <div class="containerSeccion">
					<div class="row">
						<div class="col l6 s12"> 
                            <p class="titleHeader1 blanco">¿Querés estudiar en la UCC la <br>
                                carrera de medicina?
                            </p>

                            <div class="row sabias arrow_box">
                                <div class="col l2"><span class="titleHeader2 blanco"><i>Sabías que el</i></span></div>
                                <div class="col l3 sabias"><span class="titleHeader3 blanco">63</span><span class="percent blanco">%</span></div>
                                <div class="col l6"><span class="titleHeader4 blanco"><i>de los alumnos que ingresaron a la carrera de Medicina en la UCC se <span class="bolderTitle">prepararon en Cajal Tradicional</span></i></div>
                            </div>

                            <h1 class="tituloSeccion blanco">MEDICINA</h1>
                            <hr class="lineSeccion blanco">
                            <p class="reseñaSeccion">Para comenzar con el pie derecho la facu, esta es la info que<br>
                            necesitas saber:
                            </p>
						</div>
						<div class="col l6 s12"> 
							<div class="formCursos">
								<form id="formCursos" class="col s12">
									<h2 class="titleFormSeccion">¿ESTÁS PENSANDO EN PREPARAR TU INGRESO A MEDICINA UCC CON NOSOTROS?</h2>
									<div class="row">
										<div class="input-field col s12">
											<input id="icon_prefix" name="nombreInput" type="text" class="validate">
											<label for="icon_prefix">NOMBRE</label>
											<span class="helper-text" data-error="Por favor, ingrese un nombre." data-success="" />
										</div>
										<div class="input-field col s12">
											<input id="icon_telephone" name="telefonoInput" type="tel" class="validate">
											<label for="icon_telephone">TELÉFONO</label>
											<span class="helper-text" data-error="Por favor, ingrese un telefono." data-success="" />
										</div>
										<div class="input-field col s12">
											<input id="email" name="emailInput" type="email" class="validate">
											<label for="email">EMAIL</label>
										</div>
										<div class="input-field col s12">
											<textarea id="icon_prefix2" class="materialize-textarea"></textarea>
											<label for="icon_prefix2">MENSAJE</label>
										</div>
										<button class="btn-large waves-effect ctaFormCursos celesteCTA" type="submit" name="action">
											ENVIAR CONSULTA
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

            <div class="medicinaSeccion">
                <div class="row">
                    <div class="col s6">
                        <div class="containerTextoIngreso">
                            <h1 class="tituloSeccion rojo">CURSOS<br> DE INGRESO</h1>
                            <hr class="lineSeccion rojo">
                            <p class="response">¿Cuales son las modalidades de los cursos que te brindamos desde Cajal Tradicional para preparar tu ingreso a Medicina?<br><br>
                                Desde Cajal Tradicional, entendiendo que la base de un ingreso exitoso es prepararte para cada parte de tu proceso de aprendizaje,
                                creamos un curso especialmente diseñado para que puedas ver en profundidad todos los contenidos de los programas de Ingreso a Medicina de la UCC
                                con 3 modalidades de cursado para que elijas la que mejor se adapte a tus tiempos:
                            </p>
                        </div>
                    </div>
                    <div class="col s6">
                        <img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/21751.png"> 
                    </div>
                </div>
                <hr class="lineCTA rojo">
            </div>

            <div class="modalidades row">
                    <div class="col l4">
                        <div class="rojoBackground blanco modalidad">
                            <h3 class="titleModalidad">Modalidad <br>
                                <span>ANUAL</span>
                                <hr class="lineSeccion blanco">
                            </h3>
                            <p class="reseñaSeccion">
                                Pensado para prepararte en detalle y que logres estar totalmente seguro de tus conocimientos a la hora de dar tu examen de ingreso.<br><br>
                                Inicio: Mayo<br>
                                Cursado: Sábados <br><br>
                                Materias: Química, Física y Biología.
                            </p>
                            <div class="descargaPdf">
                                <a class="waves-effect waves-light rojo CTApdf">
                                    PARA MÁS INFORMACIÓN DESCARGA EL ARCHIVO PDF<img class="imgpdf" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/pdf.svg">
                                </a>
                            </div>
                        </div>

                    </div>
                    <div class="col l4">
                        <div class="rojoBackground blanco modalidad">
                            <h3 class="titleModalidad">Modalidad <br>
                                <span>CUATRIMESTRAL</span>
                                <hr class="lineSeccion blanco">
                            </h3>
                            <p class="reseñaSeccion">
                                Pensada para prepararte en profundidad y a tus tiempos para que logres tu objetivo de ingresar a la UCC con tranquilidad.<br><br>
                                Inicio: Agosto<br>
                                Finaliza: Diciembre<br><br>
                                Materias: Química, Física y Biología.
                            </p>
                            <div class="descargaPdf">
                                <a class="waves-effect waves-light rojo CTApdf">
                                    PARA MÁS INFORMACIÓN DESCARGA EL ARCHIVO PDF<img class="imgpdf" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/pdf.svg">
                                </a>
                            </div>
                        </div>

                    </div>
                    <div class="col l4">
                        <div class="rojoBackground blanco modalidad">
                            <h3 class="titleModalidad">Modalidad <br>
                                <span>INTENSIVA</span>
                                <hr class="lineSeccion blanco">
                            </h3>
                            <p class="reseñaSeccion">
                                Pensada para prepararte de forma veloz sin perder foco en la calidad de tu aprendizaje, con un ritmo de cursado intenso.<br><br>
                                Inicio: Enero <br>
                                Finaliza: Marzo<br><br>
                                Materias: Química, Física y Biología.
                            </p>
                            <div class="descargaPdf">
                                <a class="waves-effect waves-light rojo CTApdf">
                                    PARA MÁS INFORMACIÓN DESCARGA EL ARCHIVO PDF<img class="imgpdf" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/pdf.svg">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

			<div class="contenidoSeccion">
				<div>
                    <h3 class="titleOptica rojo">¿A QUE VAS A TENER ACCESO SI PREPARAS<br> TU INGRESO EN CAJAL TRADICIONAL?</h3>
                    <hr class="lineSeccion rojo">
                    <div class="row">
                        <div class="col l6">
                            <p class="response"><i class="material-icons iconsCurso rojo">brightness_1</i> Clases presenciales en nuestras instalaciones dictadas por docentes especializados, 
                            en constante perfeccionamiento y siempre dispuestos a ayudarte en lo que necesites.</p>
                            <p class="response"><i class="material-icons iconsCurso rojo">brightness_1</i> Todas las horas de clases necesarias para afianzar tu aprendizaje de los contenidos 
                            de cada materia, de tal forma que te sientas seguro y tranquilo al momento de rendir tus exámenes.</p>
                            <p class="response"><i class="material-icons iconsCurso rojo">brightness_1</i> Exámenes de similar complejidad a los de la facu, el secreto de los temas de mayor 
                            relevancia, y las reglas memotécnicas que te permitan recordar los contenidos aprendidos.</p>
					        <p class="response"><i class="material-icons iconsCurso rojo">brightness_1</i> Método de entrenamiento Cajal.</p>
                            <p class="response"><i class="material-icons iconsCurso rojo">brightness_1</i> Ejercitación intensiva, material didáctico y apuntes propios con los mejores 
                            esquemas y cuadros sinópticos, que se actualizan cada año para facilitar tu aprendizaje.</p>
                        </div>
                        <div class="col l6">
                            <p class="response"><i class="material-icons iconsCurso rojo">brightness_1</i> Clases de consulta para que puedas despejar tus dudas.</p>
                            <p class="response"><i class="material-icons iconsCurso rojo">brightness_1</i> Aula virtual con material complementario de cada clase y evaluaciones online.</p>
                            <p class="response"><i class="material-icons iconsCurso rojo">brightness_1</i> Taller de manejo de la ansiedad: aprenderás a reforzar tus técnicas de estudio, 
                            a prepararte para saber cuál debe ser tu actitud ante los exámenes, y también cómo manejar tus tiempos para lograr mejores resultados. </p>
					        <p class="response"><i class="material-icons iconsCurso rojo">brightness_1</i> Horarios adaptables a tus tiempos de cursado.</p>
                            <p class="response"><i class="material-icons iconsCurso rojo">brightness_1</i> Modernas instalaciones en una ubicación estratégica, aulas confortables y climatizadas, 
                            a menos de 10 cuadras de la facu, en el corazón de Nueva Córdoba.</p>
                            <p class="response"><i class="material-icons iconsCurso rojo">brightness_1</i> Seguimiento personalizado de cada alumno para garantizar un mejor rendimiento. </p>
                            <p class="response"><i class="material-icons iconsCurso rojo">brightness_1</i> En caso de que el alumno no asista regularmente a clases, no estaremos poniendo en 
                            contacto con el padre o tutor.</p>
                        </div>
                    </div>
				</div>

				<div class="faqsCTA">
					<hr class="lineCTA rojo">
					<p class="titleCTA">¿Tenés alguna consulta?<br>
					¿Querés recibir info completa sobre los cursos de ingreso a Medicina UCC?</p>
					<a class="waves-effect waves-light btn CTA celesteCTA">SI QUIERO RECIBIR MÁS INFORMACIÓN<br> SOBRE EL CURSO DE INGRESO<br> A MEDICINA EN LA UCC</a>
				</div>
			</div>

<?php get_footer(); ?>