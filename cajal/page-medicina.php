<?php 
	/*
  	Template Name: Medicina Cajal Tradicional
  	*/
  	get_header(); ?>
  	<?php include('menu.php'); ?>
  	<div class="bgMedicina">
	  	<div class="volver">
			<img class="arrowBack" src="<?= get_stylesheet_directory_uri(); ?>/img/iconos/left-arrow.svg">
		</div>
	  	<div class="container">
	  		<div class="row">
	  			<div class="col s6 resultadosContent">
	  				<div class="titulo-curso">
	  					<h1>MEDICINA</h1>
	  				</div>
	  			</div>
	  			<div class="col s6">
	  				<p class="resultados" >RESULTADOS <br>
	  					UNC-2019</p>
	  				<p class="numResultados">68<span class="percent">%</span></p>
	  				<p class="resultados2"><i>de los alumnos<br> promocionados y regulares<br></i> <span class="resultados3">se prepararon en Cajal</span>.</p>
	  			</div>
	  		</div>
	  		<div class="cursoTexto">
	  			<p>Para comenzar esta etapa fundamental en tu vida, asegurá el éxito desde el comienzo, capacitándote con los mejores profesionales, que ponen a tu disposición su amplia experiencia docente, garantizando el mejor nivel académico y efectividad en tus exámenes.</p>
	  		</div>
	  		<div class="row container-medicina2">
	  			<div class="col s6">
	  				<p>Para comenzar esta etapa fundamental en tu vida, asegurá el éxito desde el comienzo, capacitándote con los mejores profesionales.</p>
	  			</div>
	  			<div class="col s6">
	  				<img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/medicina/Imagen1@2x.png">
	  			</div>
	  		</div>

	  		<div class="horarios">
	  			<h2>HORARIOS</h2>
	  			<ul class="collapsible">
				    <li>
				      	<div class="collapsible-header">MAÑANA</div>
				      	<div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
				    </li>
				</ul>
				<ul class="collapsible">
				    <li>
				      	<div class="collapsible-header">TARDE</div>
				      	<div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
				    </li>
				</ul>
	  		</div>

	  		<div class="formCurso">
	  			<form id="formConsulta" class="col s12">
                  	<div class="row">
	                    <div class="input-field col s12">
	                      	<input id="icon_prefix" name="nombreInput" type="text" class="validate">
	                      	<label for="icon_prefix">Nombre</label>
	                      	<span class="helper-text" data-error="Por favor, ingrese un nombre." data-success="" />
	                    </div>
	                    <div class="input-field col s12">
	                      	<input id="icon_telephone" name="telefonoInput" type="tel" class="validate">
	                      	<label for="icon_telephone">Telefono</label>
	                      	<span class="helper-text" data-error="Por favor, ingrese un telefono." data-success="" />
	                    </div>
	                    <div class="input-field col s12">
	                      	<input id="email" name="emailInput" type="email" class="validate">
	                      	<label for="email">Email</label>
	                      	<span class="helper-text" data-error="Por favor, ingrese un email válido." data-success="">Ej: ejemplo@ejemplo.com</span>
	                    </div>
	                    <div class="input-field col s12">
	                      	<textarea id="icon_prefix2" class="materialize-textarea"></textarea>
	                      	<label for="icon_prefix2">Deje su consulta</label>
	                    </div>
	                    <button class="btn-large waves-effect cta-form btnColorSlider1" type="submit" name="action">
	                    	ENVIAR MENSAJE
	                    </button>
                  	</div>
                </form>
	  		</div>

	  	</div>
		<footer class="page-footer">
		    <div class="row footerMedicina">
		        <div class="col s4">
		        	<img class="responsive-img" src="<?= get_stylesheet_directory_uri(); ?>/img/logoTop@2x.png">
		        </div>
		        <div class="col s6">
		        	<p class="copyright-cajal">Ingreso y apoyo Universitario en Córdoba<br> Cajal Tradicional 2019</p>
		        </div>
		    </div>
		</footer>
  	</div>

</div>



<?php get_footer(); ?>